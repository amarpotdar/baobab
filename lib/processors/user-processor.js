const util = require('../utils/util');
let userModel;
const sendSuccess = util.sendSuccess;
const sendError = util.sendError;
const validate = util.validate;
const ignore = { __v: false, _id: false };

exports.initProcessor = () => {
    userModel = require('../models/user.model');
}

exports.loginUser = (req, res) => {
    const { username, password} = req.query;
    if (!username || !password) {
        return sendError(res, 'Invalid request parameters');
    }
    return userModel.find({ username: username })
        .then(result => {
            console.log("login result..", result);
            if (result == null || !result.length) {
                throw "Incorrect username";
            } else if (result[0].password !== password) {
                throw 'Incorrect password';
            } else {
                return sendSuccess(res, result);
            }
        })
        .catch(err => {
            console.log(err);
            return sendError(res, err.toString());
        });
};

exports.createUser = (req, res) => {
    console.log(typeof (req.payload));
    console.log(req.payload);

    if (typeof (req.payload) == 'string')
        req.payload = JSON.parse(req.payload);
    if (!validate(req.payload, ['email', 'username','password', 'created_by'])) {
        return sendError(res, 'Invalid request parameters');
    }

    const ts = new Date();
    req.payload['created_on'] = ts;
    req.payload['updated_on'] = ts;
    req.payload['updated_by'] = req.payload.created_by;
    req.payload['district'] = "Pune";
    req.payload['taluka'] = "Pune";
    req.payload['city'] = "Pune";
    req.payload['status'] = 0;
    req.payload['_id'] = "UR" + util.getUniqeId(10);
    req.payload['user_id'] = req.payload['_id'];

    return userModel.find({ email: req.payload['email'] }, Object.assign({ district: false, city: false, taluka: false }, ignore))
        .then(result => {
            console.log("create user result..", result);
            if (result == null || !result.length) {
                return userModel.create(req.payload)
                    .then(result => {
                        return sendSuccess(res, result);
                    })
                    .catch(err => {
                        return sendError(res, err.toString());
                    });
            } else {
                throw "User mail id already exits";
            }
        })
        .catch(err => {
            console.log(err);
            return sendError(res, err.toString());
        });
}

exports.deleteUser = (req, res) => {
    req.payload = JSON.parse(req.payload);
    console.log("delete User request");
    console.log(req.payload);
    if (!validate(req.payload, ['user_id'])) {
        return sendError(res, 'Invalid request parameters');
    }
    return userModel.deleteOne({ _id: req.payload.user_id })
        .then(result => {
            return sendSuccess(res, result);
        })
        .catch(err => {
            return sendError(res, err.toString());
        });
}

exports.updateUser = (req, res) => {
    console.log(typeof (req.payload));
    console.log(req.payload);
    if (typeof (req.payload) == 'string')
        req.payload = JSON.parse(req.payload);
    if (!validate(req.payload, ['user_id','updated_by'])) {
        return sendError(res, 'Invalid request parameters');
    }
    const ts = new Date();
    req.payload['updated_on'] = ts;
    let _id = req.payload['user_id'];
    ['user_id'].forEach(e => delete req.payload[e]);
    if (req.payload.contact_no) {
        return userModel.find({ contact_no: req.payload['contact_no'] }, ignore)
            .then(result => {
                console.log("search client result..", result);
                if (result == null || !result.length || (result.length && result[0]['user_id'] == _id)) {
                    return userModel.updateOne({ _id: _id }, req.payload)
                        .then(result => {
                            return sendSuccess(res, result);
                        })
                        .catch(err => {
                            return sendError(res, err.toString());
                        });
                } else {
                    throw "Contact number already exits";
                }
            })
            .catch(err => {
                console.log(err);
                return sendError(res, err.toString());
            });
    } else {
        return userModel.updateOne({ _id: _id }, req.payload)
            .then(result => {
                return sendSuccess(res, result);
            })
            .catch(err => {
                return sendError(res, err.toString());
            });
    }
}

exports.getUserById = (req, res) => {
    let { user } = req.query
    if (typeof (user) == 'string')
        user = JSON.parse(user);
    console.log(user);
    if (!user.user_id || !user.user_id.length) {
        return sendError(res, 'Invalid request parameters');
    }
    return userModel.find({ '_id': { $in: user.user_id } }, Object.assign({ password: false, district: false, city: false, taluka: false }, ignore))
        .then(result => {
            return sendSuccess(res, result);
        })
        .catch(err => {
            return sendError(res, err.toString());
        });
}

exports.getAllUsers = (req, res) => {
    const {filter} = req.query;
    let filterObj = filter ? (typeof (filter) == 'string'?JSON.parse(filter):filter):{}
    if (filterObj && filterObj.search_key) {
        filterObj['$expr']= {
            "$regexMatch": {
                "input": { "$concat": ["$fname", " ", "$lname", " ", "$contact_no"] },
                "regex": search_key,
                "options": "i"
            }
        };
        delete filterObj['search_key'];
    }
    return userModel.find(filterObj).limit(50)
        .then(result => {
            return sendSuccess(res, result);
        })
        .catch(err => {
            return sendError(res, err.toString());
        });
};
