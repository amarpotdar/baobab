'use strict';

const mqtt = require('mqtt');
const host = 'broker.hivemq.com';
const port = '1883';
const connectUrl = `mqtt://${host}:${port}`
let plantProcessor, client;

exports.initProcessor = () => {

    plantProcessor = require('../processors/userplant-processor');

    client = mqtt.connect(connectUrl, {
        clean: true,
        connectTimeout: 4000,
        reconnectPeriod: 1000,
    });

    client.on('connect', function () {
        client.subscribe('test/baobab/server', function (err) {
            if (!err) {
                console.log("Server connected to MQTT.")
            } else {
                console.log(err)
            }
        })
    })

    client.on('message', function (topic, message) {
        try {
            let msgObj = JSON.parse(message.toString('utf-8'));
            console.log("msgObj ----->",msgObj);
            if (msgObj['action'] == 'init' && msgObj['device_id']) {
                sending(msgObj['device_id']);
            }
        } catch (error) {
            console.log(topic)
            console.log("mqtt ----->", error)
        }
    })
}

exports.sendData = (device_id)=>{
    sending(device_id);
}

let sending = (device_id) => {
    let obj = {};
    obj['device_id'] = device_id;
    plantProcessor.getDeviceData(obj, (results) => {
        console.log("------------------------------------------>")
        let msg = JSON.stringify(results[0]);
        let receiver = "test/baobab/"+device_id;
        console.log(receiver,"----->",msg)
        client.publish(receiver, msg);
    });
}