const util = require('../utils/util');
const mqtt = require('./mqtt-client')
let userplantsModel;
const sendSuccess = util.sendSuccess;
const sendError = util.sendError;
const sendData = mqtt.sendData;

exports.initProcessor = () => {
    userplantsModel = require('../models/userplants.model');
    
}

exports.createPlant = (req, res) => {
    console.log(typeof (req.payload));
    if (typeof (req.payload) == 'string')
        req.payload = JSON.parse(req.payload);
    let plantList = JSON.parse(req.payload.plants);
    const ts = new Date();
    let bulkWrite = [];
    let receiver = "";
    plantList.forEach((plant) => {
        plant['_id'] = plant['_id'] ? plant['_id'] : 'URPL' + util.getUniqeId(10),
            plant['updated_on'] = ts;
        plant['status'] = 1;
        console.log("plant ------------------>", plant);
        if (plant['delete']) {
            let obj = {
                'deleteOne': {
                    'filter': { '_id': plant['delete'] }
                }
            };
            bulkWrite.push(obj);
            ['delete'].forEach(e => delete plant[e]);
        }
        let obj = {
            'updateOne': {
                'filter': { '_id': plant['_id'] },
                'update': plant,
                'upsert': true
            }
        };
        receiver = plant["device_id"];
        bulkWrite.push(obj);
    });

    if (req.payload.new && req.payload.new != "") {
        console.log("****** ****** inside new device registration ********* **********");
        return userplantsModel.deleteMany({ "device_id": req.payload.new })
            .then(result => {
                console.log("****** ****** result result ********* **********", result);
                return insertPlant(res, bulkWrite,receiver);
            })
            .catch(err => {
                console.log("****** ****** Database111111111 error ********* **********", err);
                return sendError(res, err.toString());
            });
    } else {
        return insertPlant(res, bulkWrite,receiver);
    }
}

let insertPlant = (res, bulkWrite,receiver) => {
    console.log("****** ****** insertPlant insertPlant ********* **********");
    return userplantsModel.bulkWrite(bulkWrite)
        .then(result => {
            sendData(receiver);
            return sendSuccess(res, result);
        })
        .catch(err => {
            console.log("****** ****** Database 222222222 error ********* **********", err);
            return sendError(res, err.toString());
        });
}

exports.deletePlat = (req, res) => {
    console.log("delete User request");
    if (typeof (req.payload) == 'string')
        req.payload = JSON.parse(req.payload);
    console.log(req.payload);
    if (!util.validate(req.payload, ['device_id'])) {
        return sendError(res, 'Invalid request parameters');
    }
    return userplantsModel.deleteMany({ "device_id": req.payload.device_id })
        .then(result => {
            sendData(req.payload.device_id);
            return sendSuccess(res, result);
        })
        .catch(err => {
            return sendError(res, err.toString());
        });
}

exports.getAllPlants = (req, res) => {
    const { filter } = req.query;
    let filterObj = filter ? (typeof (filter) == 'string' ? JSON.parse(filter) : filter) : {};
    console.log("filterObj---------------------->", filterObj);
    return userplantsModel.aggregate(
        [
            {
                $match: filterObj
            }
        ])
        .then(result => {
            return sendSuccess(res, result);
        })
        .catch(err => {
            return sendError(res, err.toString());
        });
};

exports.getDeviceData = (filterObj,callback) => {
    console.log("filterObj ----->",filterObj);
    userplantsModel.aggregate(
        [
            {
                $match: filterObj
            }
        ])
        .then(result => {
            console.log("result ----->",result);
            if(result.length)callback(result);
            else callback(["reset"]);
        })
        .catch(err => {
            console.log("error",err)
        });
};

