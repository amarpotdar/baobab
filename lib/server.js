'use strict';

const Path = require('path');
const Hapi = require('@hapi/hapi');
const router = require('./routing/main-routing');
const constants = require('./utils/constants');
const fs = require('fs');



const serverDetails = {
    port: constants.SERVER_HTTP_PORT,
    host: constants.SERVER_HOST
}

let serverInstance;

let log = (msg) => {
    console.log("Server --------> ", msg)
}

const startHTTPServer = async (cb) => {
    serverInstance = Hapi.server({
        port: serverDetails.port,
        host: serverDetails.host,
        routes: {
            files: {
                relativeTo: Path.join(__dirname, '../public')
            }
        }
    })
    serverInstance.ext('onPreResponse', (request, reply) => {
        try {
            var response = request.response;
            log(response.output)
            response.headers['access-control-allow-origin'] = request.headers.origin;
            response.headers['access-control-expose-headers'] = 'content-type, content-length, etag';
            response.headers['access-control-max-age'] = 60 * 10; // 10 minutes
            if (request.headers['access-control-request-headers']) {
                response.headers['access-control-allow-headers'] = request.headers['access-control-request-headers']
            }
            if (request.headers['access-control-request-method']) {
                response.headers['access-control-allow-methods'] = request.headers['access-control-request-method']
            }
            log(Object.keys(reply))
            return reply.continue;
        } catch (e) {
            log(" Exception is caught");
            log(request);
            return "failed"
        }
    });
    
    await serverInstance.register(require('@hapi/inert'));

    serverInstance.route(router, {
        cors: true,
        origin: ['*'],
        headers: ['Origin', 'X-Requested-With', 'Content-Type', 'Accept'],
        methods: ["POST", "GET", "DELETE", "OPTIONS"]
    });
    await serverInstance.start();
    Object.assign(exports.myHTTPServer, { server: serverInstance })
    log(`Server running at: ${serverInstance.info.uri}`);
    if (!fs.existsSync(constants.FILE_PATH)){
        log("Error--------------------> Image path not found :");
    }
    cb(serverInstance);
}

const stopHTTPServer = () => {
    serverInstance.stop();
}

exports.myHTTPServer = {
    startServer: startHTTPServer,
    stopServer: stopHTTPServer
};