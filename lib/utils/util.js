const constants = require('./constants');

module.exports = {
    sendSuccess: (res, data) => {
        console.log(data)
        return res.response({ error: 0, message: 'Success', data: data }).code(201);
    },

    sendError: (res, msg, data) => {
        console.log(data)
        return res.response({ error: 1, message: msg, data: data });
    },

    validate: (obj, keys) => {
        for (const key of keys) {
            if (!obj[key]) { return false; }
        }
        return true;
    },

    getUniqeId(length) {
        let key = 'abcdefghijklmnopqrstuvwxyz0123456789'.split('');
        let uniqeId = "";
        for (let i = 0; i < length; i++) {
            uniqeId = uniqeId + key[Math.floor(Math.random() * 35)];
        }
        return uniqeId.toUpperCase();
    }
};