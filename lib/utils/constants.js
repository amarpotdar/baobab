'use strict'

module.exports = {
    SERVER_HTTP_PORT: 4002,
    DATABASE:"mongo",
    SERVER_HOST:"0.0.0.0",
    FILE_PATH:"./../images",
    FULL_PATH:"http://www.nms.vyaprut.com/images"
}