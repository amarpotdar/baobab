let userModel, now;
const util = require('../utils/util');
userModel = require('../models/user.model');

let log = (msg) => {
    console.log("Script --------> ", msg)
}

exports.createDefaultData = () => {
    now = new Date().toISOString();
    log("Creating default data by script @ " + now);
    createAdminUser();
}


let createAdminUser = () => {
    return userModel.create(
        {
            _id: 'UR9999999999',
            user_id: 'UR9999999999',
            contact_no: 9999999999,
            fname: 'Amar',
            lname: 'Potdar',
            email: 'dypcet2019@gmail.com',
            password: '12345',
            district: 'Pune',
            taluka: 'Haveli',
            city: 'Pune',
            updated_by: 'VA9999999999',
            created_by: 'VA9999999999',
            created_on: now,
            updated_on: now,
            status: 1
        }
    )
        .then(result => {
            log("Admin user added");
            log(result)
        })
        .catch(err => {
            log(err.toString());
        });
}

// let deletFakeUsers = (search_key) => {
//     console.log("**11111111111111*****");
//     try {
//         let filterObj = {};
//         filterObj['$expr'] = {
//             "$regexMatch": {
//                 "input": { "$concat": [" ", "$contact_no"] },
//                 "regex": search_key,
//                 "options": "i"
//             }
//         };
//         console.log(filterObj);
//         return userModel.deleteMany(filterObj)
//             .then(result => {
//                 console.log("****** ****** *******deletFakeUsers ******** ******* **********");
//                 console.log(result)
//                 console.log("****** ****** ******* ******** ******* **********");
//             })
//             .catch(err => {
//                 console.log("****** ****** ******* deletFakeUsers******** ******* **********");
//                 console.log(err)
//                 console.log("****** ****** ******* ******** ******* **********");
//             });

//     } catch (error) {
//         console.log(error)
//     }

// }