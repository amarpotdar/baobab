'use strict';

const userRouter = require('./user-routing');
const userPlantRouter= require('./userplant-routing');

module.exports = [
    {
        method: 'GET',
        path: '/{path*}',
        handler: async function (request, h) {
            console.log("Got request here....", request.url.pathname);
            let _path = request.params.path == "" ? 'index.html' : request.params.path;
            return h.file(_path);
        }
    }
].concat(userRouter,userPlantRouter);
