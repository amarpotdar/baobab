'use strict';

const userProcessor = require('../processors/user-processor');

module.exports = [
    {
        method: 'GET',
        path: '/api/user/login',
        handler: async function (request, h) {
            console.log("Got request here for /api/user/login", request.query);
            return await userProcessor.loginUser(request, h);
        }
    },
    {
        method: 'POST',
        path: '/api/user/add',
        handler: async function (request, h) {
            console.log("Got request here for /api/user/add", request.payload);
            return await userProcessor.createUser(request, h);
        }
    },
    {
        method: 'POST',
        path: '/api/user/delete',
        handler: async function (request, h) {
            console.log("Got request here for /api/user/delete", request.payload);
            return await userProcessor.deleteUser(request, h);

        }
    },
    {
        method: 'POST',
        path: '/api/user/update',
        handler: async function (request, h) {
            console.log("Got request here for /api/user/update", request.payload);
            return await userProcessor.updateUser(request, h);

        }
    },
    {
        method: 'GET',
        path: '/api/user/all',
        handler: async function (request, h) {
            console.log("Got request here for /api/user/all", request.query);
            return await userProcessor.getAllUsers(request, h);
        }
    },
    {
        method: 'GET',
        path: '/api/user/id',
        handler: async function (request, h) {
            console.log("Got request here for /api/user/id", request.query);
            return await userProcessor.getUserById(request, h);
        }
    }
]