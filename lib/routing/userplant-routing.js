'use strict';

const plantProcessor = require('../processors/userplant-processor');

module.exports = [
    {
        method: 'POST',
        path: '/api/userplant/add',
        handler: async function (request, h) {
            console.log("Got request here for /api/userplant/add", request.payload);
            return await plantProcessor.createPlant(request, h);
        }
    },
    {
        method: 'GET',
        path: '/api/userplant/all',
        handler: async function (request, h) {
            console.log("Got request here for /api/userplant/all", request.query);
            return await plantProcessor.getAllPlants(request, h);
        }
    },
    {
        method: 'POST',
        path: '/api/userplant/update',
        handler: async function (request, h) {
            console.log("Got request here for /api/userplant/update", request.payload);
            return await plantProcessor.updatePlant(request, h);
        }
    },
    {
        method: 'POST',
        path: '/api/userplant/delet',
        handler: async function (request, h) {
            console.log("Got request here for /api/userplant/delet", request.payload);
            return await plantProcessor.deletePlat(request, h);
        }
    },
]