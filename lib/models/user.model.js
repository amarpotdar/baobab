const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    _id: {
        required: true,
        type: String
    },
    user_id:String,
    contact_no: String,
    username:String,
    fname: String,
    lname: String,
    email: String,
    password: String,
    district: String,
    taluka : String,
    city: String,
    fcm_token:String,
    updated_by: String,
    created_by: String,
    created_on : Date,
    updated_on : Date,
    status: Number
},{
    timestamps: false
});
module.exports = mongoose.model('users', userSchema);