const mongoose = require('mongoose');
//get the Schema class
const Schema = mongoose.Schema;

const userPlantsSchema = new Schema({
    _id: {
        required: true,
        type: String
    },
    device_id: String,
    user_id: String,
    logo: String,
    title: String,
    duration: Number,
    start_time_hr: Number,
    start_time_min: Number,
    updated_by: String,
    updated_on: Date,
    status: Number
},
    {
        timestamps: false
    });

module.exports = mongoose.model('userplants', userPlantsSchema);