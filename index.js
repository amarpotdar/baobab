'use strict'
const userProc = require('./lib/processors/user-processor');
const userplantProc = require('./lib/processors/userplant-processor');
const mongodb = require('./lib/utils/mongodb');
const constants = require('./lib/utils/constants');
const scriptDoc = require('./lib/utils/script');
const app = require('./lib/server');
const mqtt = require('./lib/processors/mqtt-client')

let serverInstance;

//First create server then talk blah blah blah
app.myHTTPServer.startServer((si) => {
    serverInstance = si;
    if (constants.DATABASE == "mongo") {
        mongodb.initDatabase(() => {
            setTimeout(() => {
                scriptDoc.createDefaultData();
                userProc.initProcessor();
                userplantProc.initProcessor();
                mqtt.initProcessor();
            }, 1000);
        })
    }
})